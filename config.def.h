/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 0;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 0;        /* 0 means bottom bar */
static const char *fonts[]          = { "Hack:size=9", "Unifont:size=12" };
static const char dmenufont[]       = "Hack:size=9";
static const char col_black[]	    = "#000000";
static const char col_gray0[]	    = "#111111";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeInv]  = { col_gray3, col_gray0, col_black },
	[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
	[SchemeTabActive]  = { col_gray3, col_gray1,  col_black },
	[SchemeTabInactive]  = { col_gray3, col_gray1,  col_black }
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class     instance  title           tags mask  isfloating  isterminal  noswallow  monitor */
	{ "Gimp",    NULL,     NULL,           0,         0,          0,           0,        -1 },
	{ "Firefox", NULL,     NULL,           0,         0,          0,          -1,        -1 },
	{ "st",      NULL,     NULL,           0,         0,          1,           0,        -1 },
	{ "Pavucontrol", NULL, NULL,           1 << 1,    0,          0,           0,        -1 },
	{ "discord", NULL,     NULL,           1 << 1,    0,          0,           0,        -1 },
	{ NULL,      NULL,     "Event Tester", 0,         0,          0,           1,        -1 }, /* xev */
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

#include "fibonacci.c"
#include "simplecols.c"

/* Bartabgroups properties */
#define BARTAB_BORDERS 1       // 0 = off, 1 = on
#define BARTAB_BOTTOMBORDER 0  // 0 = off, 1 = on
#define BARTAB_TAGSINDICATOR 0 // 0 = off, 1 = on if >1 client/view tag, 2 = always on
#define BARTAB_TAGSPX 5        // # pixels for tag grid boxes
#define BARTAB_TAGSROWS 3      // # rows in tag grid (9 tags, e.g. 3x3)
static void (*bartabmonfns[])(Monitor *) = { monocle, simplecols /* , customlayoutfn */ };
static void (*bartabfloatfns[])(Monitor *) = { NULL /* , customlayoutfn */ };

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
 	{ "[@]",      spiral },
 	{ "[\\]",     dwindle },
 	{ "|||",      simplecols },
 	{ "[D]",      doubledeck },
 	{ "TTT",      bstack },
};

static void viewmon(const Arg *arg) {
    focusmon(arg);
    view(arg);
}

static void tagviewmon(const Arg *arg) {
    unsigned int n = selmon->num;
    tagmon(arg);
    if(n == selmon->num)
	focusmon(arg);
    tag(arg); view(arg);
}

static void tagmonmove(const Arg *arg) {
    unsigned int n = selmon->num;
    tagmon(arg);
    if(n == selmon->num) {
	focusmon(arg);
    }
}
	/* { MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } }, */
static void tagmondontmove(const Arg *arg) {
    unsigned int n = selmon->num;
    tagmon(arg);
    if(n != selmon->num) {
	Arg a;
	a.i = -arg->i;
	focusmon(&a);
    }
}

static void viewadjacent(const Arg *arg) {
	unsigned int st = selmon->tagset[selmon->seltags];
	Arg a;
	if (st == (arg->i < 0 ? 1 : 1 << 8))
		return;

	a.ui = arg->i < 0 ? st >> 1 : st << 1;
	view(&a);
}

static void viewadjoccupied(const Arg *arg) {
	unsigned int i, occ = 0;
	unsigned int st = selmon->tagset[selmon->seltags];
	Client *c;
	Arg a = { .ui = st };

	for (c = selmon->clients; c; c = c->next)
		occ |= c->tags == 255 ? 0 : c->tags;

	for (i = 1; i < 9; i++) {
		if(((arg->i < 0 ? (st >> i) : (st << i)) & occ) > 0) {
			a.ui = arg->i < 0 ? (st >> i) : (st << i);
			break;
		}
	}

	if (a.ui == st)
		return;

	view(&a);
}

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      viewmon,        {.i = 1, .ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      tagviewmon,     {.i = 1, .ui = 1 << TAG} },

	/* { MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \ */
/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-f", "-m", dmenumon, NULL };
static const char *termcmd[]  = { "st", NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_d,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_KP_Enter, spawn,        {.v = termcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_j,      pushdown,       {0} },
	{ MODKEY|ShiftMask,             XK_k,      pushup,         {0} },
	{ MODKEY|ShiftMask,             XK_h,      incnmaster,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_l,      incnmaster,     {.i = +1 } },
	{ MODKEY,             XK_i,      incnmaster,     {.i = -1 } },
	{ MODKEY,             XK_o,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_m,      focusmaster,    {0} },
	{ MODKEY,			XK_n,      zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_q,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[4]} },
	{ MODKEY,                       XK_e,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_c,      setlayout,      {.v = &layouts[5]} },
	{ MODKEY,                       XK_r,      setlayout,      {.v = &layouts[6]} },
	{ MODKEY,                       XK_y,      setlayout,      {.v = &layouts[7]} },
	{ MODKEY|ShiftMask,             XK_f,      fullscreen,     {0} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmondontmove,       {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmondontmove,       {.i = +1 } },
	{ MODKEY,                       XK_semicolon,    focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_apostrophe,   focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_semicolon,    tagmonmove,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_apostrophe,   tagmonmove,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_bracketleft,  viewadjacent,   {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_bracketright, viewadjacent,   {.i = +1 } },
	{ MODKEY,             XK_bracketleft,  viewadjoccupied,    {.i = -1 } },
	{ MODKEY,             XK_bracketright, viewadjoccupied,    {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_Escape,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	/* { ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} }, */
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkWinTitle,          0,              Button4,        focusstack,     {.i = -1} },
	{ ClkWinTitle,          0,              Button5,        focusstack,     {.i = +1} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button4,        viewadjoccupied,    {.i = -1} },
	{ ClkTagBar,            0,              Button5,        viewadjoccupied,    {.i = +1} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

