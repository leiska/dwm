void
simplecols(Monitor *m)
{
	unsigned int i, n, w, x, mw;
	Client *c;

	for (n = 0, c = nexttiled(m->clients); c; c = nexttiled(c->next), n++);
	if (n == 0)
		return;
	mw = m->ww;

	for(i = x = 0, c = nexttiled(m->clients); c; c = nexttiled(c->next), i++) {
		w = (mw - x) / (n-i);
		resize(c, x + m->wx, m->wy, w - (2*c->bw), m->wh - (2*c->bw), False);
		x += WIDTH(c);
	}
}
